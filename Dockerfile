FROM docker.jianmuhub.com/library/alpine:3.17.0
ARG TARGETOS
ARG TARGETARCH
ADD ./${TARGETOS}/${TARGETARCH}/jianmu-worker-kube /jianmu-worker-kube
ADD version /version
ENTRYPOINT ["/jianmu-worker-kube"]
CMD ["daemon"]