// Copyright (c) 2022 by Duguang.IO Inc. All Rights Reserved.
// Author: Ethan Liu
// Date: 2022-05-10 18:39:27

package client

import (
	"context"
	"errors"
	"jianmu-worker-kube/engine"
)

// ErrOptimisticLock is returned by if the struct being
// modified has a Version field and the value is not equal
// to the current value in the database
var ErrOptimisticLock = errors.New("optimistic lock error")

type (
	Filter struct {
		WorkerId string            `json:"workerId"`
		Type     string            `json:"type"`
		Labels   map[string]string `json:"labels,omitempty"`
	}
)

type Client interface {
	// Ping ping服务器
	Ping(ctx context.Context) error
	// Join join服务器
	Join(ctx context.Context, worker *engine.Worker) error
	// Request 获取新任务执行
	Request(ctx context.Context, args *Filter) (*engine.Unit, error)
	// Accept 确认任务执行
	Accept(ctx context.Context, runner *engine.Runner) error
	// FindById 根据任务ID查找任务
	FindById(ctx context.Context, id string) (*engine.Unit, error)
	// Update 更新任务状态
	Update(ctx context.Context, task *engine.Task) error
	// Watch 监听任务是否取消
	Watch(ctx context.Context, taskId string) (bool, error)
	// Batch 实时日志批量上传
	Batch(ctx context.Context, taskId string, lines []*engine.Line) error
	// Upload 完整日志上传
	Upload(ctx context.Context, taskId string, lines []*engine.Line) error
}
