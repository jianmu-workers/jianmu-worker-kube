// Copyright (c) 2022 by Duguang.IO Inc. All Rights Reserved.
// Author: Ethan Liu
// Date: 2022-05-14 23:00:20

package client

import (
	"context"
	"jianmu-worker-kube/engine"
	"testing"
	"time"
)

func TestRequest(t *testing.T) {
	var nocontext = context.Background()
	c := New("http://127.0.0.1:8081", "token123", false, "worker123")
	_, err := c.Request(nocontext, nil)
	if err != nil {
		t.Log(err)
	}
}

func TestUpdate(t *testing.T) {
	var nocontext = context.Background()
	c := New("http://127.0.0.1:8081", "token123", false, "worker123")
	ts := &engine.Task{
		TaskId:     "task456",
		Status:     engine.Failed,
		ExitCode:   1,
		ResultFile: "result12",
		ErrorMsg:   "error321",
	}
	err := c.Update(nocontext, ts)
	if err != nil {
		t.Log(err)
	}
}

func TestUpload(t *testing.T) {
	var nocontext = context.Background()
	c := New("http://127.0.0.1:8081", "token123", false, "worker123")
	ti := time.Now()
	line := &engine.Line{
		Number:    1,
		Message:   "test log",
		Timestamp: ti.Unix(),
	}
	err := c.Upload(nocontext, "task456", []*engine.Line{line})
	if err != nil {
		t.Log(err)
	}
}

func TestBatch(t *testing.T) {
	var nocontext = context.Background()
	c := New("http://127.0.0.1:8081", "token123", false, "worker123")
	ti := time.Now()
	line := &engine.Line{
		Number:    2,
		Message:   "test2 log",
		Timestamp: ti.Unix(),
	}
	err := c.Batch(nocontext, "task4562", []*engine.Line{line})
	if err != nil {
		t.Log(err)
	}
}
