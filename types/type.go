// Copyright (c) 2022 by Duguang.IO Inc. All Rights Reserved.
// Author: Ethan Liu
// Date: 2022-06-27 08:52:02

package types

type (
	// Secret is an interface that must be implemented
	// by all pipeline secrets.
	Secret interface {
		// GetName returns the secret name.
		GetName() string

		// GetValue returns the secret value.
		GetValue() string

		// IsMasked returns true if the secret value should
		// be masked. If true the secret value is masked in
		// the logs.
		IsMasked() bool
	}
)
