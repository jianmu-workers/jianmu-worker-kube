// Copyright (c) 2022 by Duguang.IO Inc. All Rights Reserved.
// Author: Ethan Liu
// Date: 2022-06-05 09:22:49

package command

import (
	"github.com/alecthomas/kong"
)

var cli struct {
	Daemon DaemonCmd `cmd:"" help:"starts the worker daemon."`
}

// Command 解析命令行参数并执行一个子命令程序
func Command() {
	ctx := kong.Parse(&cli)
	// Call the Run() method of the selected parsed command.
	err := ctx.Run()
	ctx.FatalIfErrorf(err)
}
