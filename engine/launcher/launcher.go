// Copyright (c) 2022 by Duguang.IO Inc. All Rights Reserved.
// Author: Ethan Liu
// Date: 2022-06-26 22:02:16

package launcher

import (
	"context"
	"fmt"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/wait"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/util/retry"
)

const aggregateTimer = 400 * time.Millisecond

// Launcher 可以一次启动多个容器，使用定时器来收集容器启动事件
// 多个容器的更新事件只需要调用一次Kubernetes API
type Launcher struct {
	stop, stopped chan struct{}

	kubeClient  kubernetes.Interface
	podUpdateMx *sync.Mutex

	podNamespace string
	podName      string

	timer     *time.Timer
	requests  map[string]*request
	requestCh chan request
}

type request struct {
	containerID    string
	containerImage string
	chErr          chan error
	statusEnvs     map[string]string
	found          bool
}

// New 创建Launcher
func New(podName, podNamespace string, clientset kubernetes.Interface, podUpdateMx *sync.Mutex) *Launcher {
	l := &Launcher{
		stop:         make(chan struct{}),
		stopped:      make(chan struct{}),
		kubeClient:   clientset,
		podUpdateMx:  podUpdateMx,
		podNamespace: podNamespace,
		podName:      podName,
		timer:        nil,
		requests:     nil,
		requestCh:    make(chan request),
	}

	return l
}

// Stop 终止Launcher的主协程
func (l *Launcher) Stop() {
	close(l.stop)
	<-l.stopped
}

// Start 启动Launcher的主协程
func (l *Launcher) Start(ctx context.Context) {
	if l.timer != nil {
		panic("timer already started")
	}

	t := time.NewTimer(time.Second)
	t.Stop()

	l.timer = t

	go func() {
		defer close(l.stopped)

		for {
			select {
			case <-ctx.Done():
				return

			case <-l.stop:
				return

			case <-l.timer.C:
				l.startContainers(ctx, l.requests)
				l.requests = nil

			case req := <-l.requestCh:
				if l.requests == nil {
					l.requests = make(map[string]*request)
				}
				l.requests[req.containerID] = &req

				if !l.timer.Stop() {
					select {
					default:
					case <-l.timer.C:
					}
				}

				l.timer.Reset(aggregateTimer)
			}
		}
	}()
}

// Launch schedules launch of a pod's container.
func (l *Launcher) Launch(containerID, containerImage string, statusEnvs map[string]string) <-chan error {
	chErr := make(chan error)
	l.requestCh <- request{
		containerID:    containerID,
		containerImage: containerImage,
		chErr:          chErr,
		statusEnvs:     statusEnvs,
		found:          false,
	}

	return chErr
}

func (l *Launcher) startContainers(ctx context.Context, requests map[string]*request) {
	var backoff = wait.Backoff{
		Steps:    15,
		Duration: 500 * time.Millisecond,
		Factor:   1.0,
		Jitter:   0.5,
	}

	t := time.Now()

	err := retry.RetryOnConflict(backoff, func() error {
		// We protect this read/modify/write with a mutex to reduce the
		// chance of a self-inflicted concurrent modification error
		// when a DAG in a pipeline is fanning out and we have a lot of
		// steps to Start at once.
		l.podUpdateMx.Lock()
		defer l.podUpdateMx.Unlock()

		configMap, err := l.kubeClient.CoreV1().ConfigMaps(l.podNamespace).Get(ctx, l.podName, metav1.GetOptions{})
		if err != nil {
			return err
		}
		pod, err := l.kubeClient.CoreV1().Pods(l.podNamespace).Get(ctx, l.podName, metav1.GetOptions{})
		if err != nil {
			return err
		}

		for i, container := range pod.Spec.Containers {
			req, ok := requests[container.Name]
			if !ok {
				continue
			}

			req.found = true

			pod.Spec.Containers[i].Image = req.containerImage

			if pod.ObjectMeta.Annotations == nil {
				pod.ObjectMeta.Annotations = map[string]string{}
			}
			for envName, envValue := range req.statusEnvs {
				pod.ObjectMeta.Annotations[envName] = envValue
				configMap.Data[l.podName+"."+envName] = envValue
			}
		}

		_, err = l.kubeClient.CoreV1().ConfigMaps(l.podNamespace).Update(ctx, configMap, metav1.UpdateOptions{})
		if err != nil {
			return err
		}

		_, err = l.kubeClient.CoreV1().Pods(l.podNamespace).Update(ctx, pod, metav1.UpdateOptions{})
		return err
	})
	if err != nil {
		logrus.
			WithError(err).
			Errorf("Launch of %d containers failed. Duration=%.2fs", len(requests), time.Since(t).Seconds())

		for _, req := range requests {
			req.chErr <- err
		}

		return
	}

	var notFounds int
	for _, req := range requests {
		if !req.found {
			notFounds++
		}
	}

	logrus.
		WithField("count", len(requests)).
		WithField("failed", notFounds).
		WithField("success", len(requests)-notFounds).
		Debugf("Launched containers. Duration=%.2fs", time.Since(t).Seconds())

	for _, req := range requests {
		if !req.found {
			req.chErr <- fmt.Errorf("container %s not found in pod %s", req.containerID, l.podName)
			continue
		}

		req.chErr <- nil
	}
}
