# jianmu-worker-kube

#### 介绍
建木Kubernetes Worker

## 如何编译

确定Golang版本为 1.18.1

第一步：使用 `go mod download` 命令下载依赖包（注意：不能下载原因可以先Google一下，解决能Google的问题再下载！）

第二步：在项目根目录下使用 `go build` 命令进行编译，编译完成后根目录下将生成名为 `jianmu-worker-kube` 的可执行文件

## 如何使用

